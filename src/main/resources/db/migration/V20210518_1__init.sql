ALTER TABLE message
    ADD COLUMN created_timestamp bigint ,
    ADD COLUMN updated_timestamp bigint ;
