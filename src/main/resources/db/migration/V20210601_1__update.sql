alter table message add column isRead boolean;
alter table message add column readTimestamp bigint;
alter table message add column isDelivered boolean;
alter table message add column deliveredTimestamp bigint;