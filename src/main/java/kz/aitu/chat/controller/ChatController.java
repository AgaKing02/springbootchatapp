package kz.aitu.chat.controller;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/chats")
@AllArgsConstructor
public class ChatController {
    private final ChatService chatService;


    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatService.findAll());
    }

    @GetMapping("/{id}/users")
    public ResponseEntity<?> getAllUsersByChatID(@PathVariable(name = "id") Long id) {
        try {
            return ResponseEntity.ok(chatService.getAllUsersByChatId(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Chat does not exist " + id);
        }
    }


    @PostMapping
    public ResponseEntity<?> saveChat(@RequestBody Chat message) {
        return ResponseEntity.ok(chatService.save(message));
    }


    @PutMapping
    public ResponseEntity<?> updateChat(@RequestBody Chat message) {
        return ResponseEntity.ok(chatService.save(message));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteChat(@RequestParam(name = "id") Long id) {
        try {
            chatService.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("Chat not found or deleted " + id);
        }
        return ResponseEntity.ok("Chat successfully Deleted:");
    }


}
