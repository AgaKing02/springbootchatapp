package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.AuthDto;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.UsersRepository;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private final AuthService authService;
    private final UsersRepository usersRepository;


    @PostMapping
    public ResponseEntity<?> authentication(@RequestBody Auth auth, @RequestParam(name = "username") String username) {
        Users user = new Users(username);
        return ResponseEntity.ok("User created");
    }

    @GetMapping
    public ResponseEntity<?> authorization(@RequestBody AuthDto authDto) {
        try {
            return ResponseEntity.ok(authService.authorize(authDto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

}
