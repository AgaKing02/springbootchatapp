package kz.aitu.chat.model;

import lombok.Data;

@Data
public class AuthDto {
    private String login;
    private String password;
}
