package kz.aitu.chat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private Long chatId;
    private String text;
    private Long createdTimestamp;
    private Long updatedTimestamp;
    @Column(columnDefinition = "boolean default false")
    private boolean isread;
    @Column(name = "is_delivered",columnDefinition = "boolean default false")
    private boolean isdelivered;
    @Column(name = "readtimestamp")
    private Long readTimestamp;
    @Column(name = "deliveredtimestamp")
    private Long deliveredTimestamp;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(32) default 'BASIC'",name = "type")
    private MessageType messageType;



}
