package kz.aitu.chat.model;

public enum ParticipantStatus {
    TYPING,ONLINE,OFFLINE
}
