package kz.aitu.chat.service;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.AuthDto;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.AuthRepository;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UsersRepository usersRepository;

    public Auth getByToken(String token) {
        return authRepository.getByToken(token);
    }

    public void authenticate(Auth auth, Users user) {
        Users newUser = usersRepository.save(user);

        Long time = new Date().getTime();

        auth.setLastLoginTimestamp(time);
        auth.setUserId(newUser.getId());
        save(auth);
    }

    public Auth save(Auth auth) {
        return authRepository.save(auth);
    }

    public String authorize(AuthDto authDto) {
        Auth auth = authRepository.getByLoginAndPassword(authDto.getLogin(), authDto.getPassword());
        UUID token = UUID.randomUUID();

        auth.setToken(token.toString());
        return authRepository.save(auth).getToken();
    }
}
