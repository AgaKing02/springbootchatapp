package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.MessageType;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.ParticipantStatus;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    private final ParticipantService participantService;

    public List<Message> findLastMessagesByChatId(Long chatId, Long userId) {
        setStatusToUser(userId, ParticipantStatus.ONLINE);
        return messageRepository.findAllByChatId(chatId, PageRequest.of(1, 10, Sort.by(Sort.Direction.DESC, "created_timestamp")).first());
    }

    public List<Message> findAllNotDeliveredMessage(Long chatId, Long userId) {
        setStatusToUser(userId, ParticipantStatus.ONLINE);
        return messageRepository.getAllByChatIdAndUserIdNotAndIsdeliveredFalse(chatId, userId);
    }


    public List<Message> findAllByChatId(Long chatId, Long userId) {
        List<Message> messages = messageRepository.findAllByChatId(chatId);
        for (Message message : messages) {
            if (!message.getUserId().equals(userId) && !message.isIsread()) {
                message.setIsread(true);
            }
        }
        return messageRepository.saveAll(messages);
    }

    public Message addMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() != null) throw new Exception("message has id");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        if (!participantService.existsByChatIdAndUserId(message.getChatId(), message.getUserId())) {
            throw new Exception("Forbidden");
        }
        Long date = new Date().getTime();
        message.setCreatedTimestamp(date);
        message.setMessageType(MessageType.BASIC);

        message.setUpdatedTimestamp(date);

        message.setIsdelivered(true);
        message.setDeliveredTimestamp(date);


        return messageRepository.save(message);
    }

    public Message updateMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() == null) throw new Exception("message id is null");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        if (!participantService.existsByChatIdAndUserId(message.getChatId(), message.getUserId())) {
            throw new Exception("Forbidden");
        }
        Optional<Message> messageDB = messageRepository.findById(message.getId());
        if (messageDB.isEmpty()) throw new Exception("message not found");

        String text = message.getText();
        message = messageDB.get();
        Long date = new Date().getTime();
        message.setText(text);
        message.setUpdatedTimestamp(date);
        message.setMessageType(MessageType.UPDATED);

        message.setIsdelivered(true);
        message.setDeliveredTimestamp(date);

        return messageRepository.save(message);
    }

    public void logout(Long userId) {
        setStatusToUser(userId, ParticipantStatus.OFFLINE);
    }

    public void setStatusToUser(Long userId, ParticipantStatus participantStatus) {
        Participant participant = participantService.getByChatId(userId);
        participant.setStatus(participantStatus);
        participantService.save(participant);
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }
}
