package kz.aitu.chat.service;

import kz.aitu.chat.model.Participant;
import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ParticipantService {
    private final ParticipantRepository participantRepository;

    public boolean existsByChatIdAndUserId(Long chatId, Long userId) {
        return participantRepository.existsByChatIdAndUserId(chatId, userId);
    }
    public Participant getByChatId(Long userId){
        return participantRepository.getByUserId(userId);
    }
    public void save(Participant participant){
        participantRepository.save(participant);
    }

}
