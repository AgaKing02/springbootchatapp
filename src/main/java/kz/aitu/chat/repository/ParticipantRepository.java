package kz.aitu.chat.repository;

import kz.aitu.chat.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {
    @Query("select o from Participant o where o.chatId=?1")
    List<Participant> getAllByChat_id(Long id);
    boolean existsByChatIdAndUserId(Long chatId, Long userId);
    Participant getByUserId(Long userId);

}